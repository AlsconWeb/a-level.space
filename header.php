<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package A-Level_space
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#000">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="alevel" class="site">

	<header id="header" class="header">
        <div class="container">
            <a href="<?php echo get_home_url(); ?>" class="logo">
                <img src="<?php echo  get_template_directory_uri() . '/build/images/alevelLogo.png' ?>" alt="Logo">
            </a>
            <nav id="site-navigation" class="main-navigation">
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-1',
                        'menu_id'        => 'primary-menu',
                    )
                );
                ?>
            </nav><!-- #site-navigation -->

            <div class="mob-menu-btn">
                <i class="fas fa-bars"></i>
            </div>
        </div>
	</header><!-- #header -->

    <div class="menu-modal">
        <div class="modal-content">
            <div class="menu-modal-close"><i class="fas fa-times"></i></div>
            <?php
            wp_nav_menu(
                array(
                    'theme_location' => 'menu-1',
                    'menu_id'        => 'primary-menu',
                )
            );
            ?>
        </div>
    </div>

    <div class="site-content">

        <fieldset class="hr-left">
            <legend>Education space</legend>
        </fieldset>
        <hr class="hr-right"/>

