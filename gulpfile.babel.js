import gulp from 'gulp';
import sass from 'gulp-sass';
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import rename from 'gulp-rename';
import cleanCSS from 'gulp-clean-css';
import autoprefixer from 'gulp-autoprefixer';
import imagemin from 'gulp-imagemin';
import del from 'del';

const paths = {
	styles: {
		src: './assets/scss/**/*.scss',
		dest: './build/styles/',
	},
	scripts: {
		src: './assets/js/**/*.js',
		dest: './build/js/',
	},
	images: {
		src: './assets/images/**/*.{jpg,jpeg,png}',
		dest: './build/images/',
	},
};

/*
 * For small tasks you can export arrow functions
 */
export const clean = () => del( [ 'assets' ] );

/*
 * You can also declare named functions and export them as tasks
 */
export function styles() {
	return gulp.src( paths.styles.src )
		.pipe( sass({outputStyle: 'compressed'}).on('error', sass.logError) )
		.pipe(autoprefixer({
			cascade: false
		}))
		.pipe( rename( {
			suffix: '.min',
		} ) )
		.pipe( gulp.dest( paths.styles.dest ) );
}

export function scripts() {
	return gulp.src( paths.scripts.src, { sourcemaps: true } )
		.pipe( babel() )
		.pipe( uglify() )
		.pipe( concat( 'main.min.js' ) )
		.pipe( gulp.dest( paths.scripts.dest ) );
}

export function images() {
	return gulp.src( paths.images.src, { since: gulp.lastRun( images ) } )
		.pipe( imagemin() )
		.pipe( gulp.dest( paths.images.dest ) );
}

/*
 * You could even use `export as` to rename exported tasks
 */
function watchFiles() {
	gulp.watch( paths.scripts.src, scripts );
	gulp.watch( paths.styles.src, styles );
	gulp.watch( paths.images.src, images );
}
export { watchFiles as watch };

const build = gulp.series( gulp.parallel( styles, scripts, images ) );

/*
 * Export a default task
 */
export default build;
