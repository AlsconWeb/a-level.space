<?php
/**
 * A-Level space functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package A-Level_space
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'alevel_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function alevel_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on A-Level space, use a find and replace
		 * to change 'alevel' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'alevel', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			[
				'menu-1' => esc_html__( 'Primary', 'alevel' ),
			]
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			[
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			]
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'alevel_custom_background_args',
				[
					'default-color' => 'ffffff',
					'default-image' => '',
				]
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			[
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			]
		);
	}
endif;
add_action( 'after_setup_theme', 'alevel_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function alevel_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'alevel_content_width', 640 );
}

add_action( 'after_setup_theme', 'alevel_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function alevel_widgets_init() {
	register_sidebar(
		[
			'name'          => esc_html__( 'Sidebar', 'alevel' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'alevel' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		]
	);
}

add_action( 'widgets_init', 'alevel_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function alevel_scripts() {
	wp_enqueue_style( 'alevel-style', get_stylesheet_uri(), [], _S_VERSION );
	wp_enqueue_style( 'alevel-slick-style', get_template_directory_uri() . '/assets/slick/slick.css', [], _S_VERSION );
	wp_enqueue_style( 'alevel-slick-theme-style', get_template_directory_uri() . '/assets/slick/slick-theme.css', [], _S_VERSION );

	wp_enqueue_style( 'alevel-fonts-style', get_template_directory_uri() . '/build/styles/fonts.min.css', [ 'alevel-global-style' ], _S_VERSION );
	wp_enqueue_style( 'alevel-global-style', get_template_directory_uri() . '/build/styles/global.min.css', [], _S_VERSION );
	wp_enqueue_style( 'alevel-home-style', get_template_directory_uri() . '/build/styles/home.min.css', [], _S_VERSION );
	wp_enqueue_style( 'alevel-arenda-style', get_template_directory_uri() . '/build/styles/arenda.min.css', [], _S_VERSION );

	wp_enqueue_style( 'alevel-icon-style', get_template_directory_uri() . '/assets/fonts/fontawesome/css/all.min.css', [], _S_VERSION );

	wp_enqueue_script( 'alevel-slick-js', get_template_directory_uri() . '/assets/slick/slick.min.js', [ 'jquery' ], _S_VERSION, true );
	wp_enqueue_script( 'alevel-mask-js', get_template_directory_uri() . '/assets/mask/src/jquery.mask.js', [ 'jquery' ], _S_VERSION, true );

	wp_enqueue_script( 'alevel-main-js', get_template_directory_uri() . '/build/js/main.min.js', [ 'jquery' ], _S_VERSION, true );
}

add_action( 'wp_enqueue_scripts', 'alevel_scripts' );


/**
 * Add active class to menu item.
 *
 * @param array $classes Array classes.
 *
 * @return array
 */
function special_nav_class( $classes ) {
	if ( in_array( 'current-menu-item', $classes, true ) ) {
		$classes[] = 'active ';
	}

	return $classes;
}

add_filter( 'nav_menu_css_class', 'special_nav_class' );

add_filter(
	'excerpt_length',
	function () {
		return 30;
	}
);


/**
 * Add custom fields.
 */

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * Add carbon theme options.
 */
function crb_attach_theme_options() {
	Container::make( 'theme_options', 'Theme Options' )
		->add_tab(
			__( 'Контакты' ),
			[
				Field::make( 'text', 'crb_address', __( 'Адрес', 'alevel' ) )
					->set_default_value( 'Харьков <br><span>пл. Павловская 6</span>' )
					->set_width( 100 ),
				Field::make( 'text', 'crb_phone', __( 'Телефон Харьков', 'alevel' ) )
					->set_default_value( '+89151671884' )
					->set_width( 33 ),
				Field::make( 'text', 'crb_phone_kiev', __( 'Телефон Киев', 'alevel' ) )
					->set_default_value( '+89151671884' )
					->set_width( 33 ),
				Field::make( 'text', 'crb_email', __( 'E-mail', 'alevel' ) )
					->set_default_value( 'zakaz@housecontrol.ru' )
					->set_width( 33 ),
			]
		)
		->add_tab(
			__( 'Социальные сети' ),
			[
				Field::make( 'text', 'crb_instagram', __( 'Instagram', 'alevel' ) )
					->set_width( 33 ),
				Field::make( 'text', 'crb_telegram_s', __( 'Telegram', 'alevel' ) )
					->set_width( 33 ),
				Field::make( 'text', 'crb_facebook', __( 'Facebook', 'alevel' ) )
					->set_width( 33 ),
			]
		)->add_tab(
			__( 'Галерея' ),
			[
				Field::make( 'image', 'crb_image1', __( 'Фото', 'alevel' ) )->set_width( 25 )->set_value_type( 'url' ),
				Field::make( 'image', 'crb_image2', __( 'Фото', 'alevel' ) )->set_width( 25 )->set_value_type( 'url' ),
				Field::make( 'image', 'crb_image3', __( 'Фото', 'alevel' ) )->set_width( 25 )->set_value_type( 'url' ),
				Field::make( 'image', 'crb_image4', __( 'Фото', 'alevel' ) )->set_width( 25 )->set_value_type( 'url' ),
				Field::make( 'image', 'crb_image5', __( 'Фото', 'alevel' ) )->set_width( 33 )->set_value_type( 'url' ),
				Field::make( 'image', 'crb_image6', __( 'Фото', 'alevel' ) )->set_width( 33 )->set_value_type( 'url' ),
				Field::make( 'image', 'crb_image7', __( 'Фото', 'alevel' ) )->set_width( 33 )->set_value_type( 'url' ),
			]
		)->add_tab(
			__( 'Telegram' ),
			[
				Field::make( 'complex', 'crb_telegram', __( 'Telegram users', 'alevel' ) )->add_fields(
					[
						Field::make( 'text', 'crb_id', __( 'ID', 'alevel' ) )
							->set_width( 50 ),
						Field::make( 'text', 'crb_name', __( 'Name', 'alevel' ) )
							->set_width( 50 ),
					]
				),
			]
		)->add_tab(
			__( 'форма успеха' ),
			[
				Field::make( 'rich_text', 'arenda_form', __( 'Аренда', 'alevel' ) )
					->set_default_value( '<h3>Заявка <span>отправлена.</span></h3><p>Мы скоро с вами свяжемся.</p>' )
					->set_width( 50 ),
				Field::make( 'rich_text', 'event_form', __( 'Ивенты', 'alevel' ) )
					->set_default_value( '<h3>Заявка <span>отправлена.</span></h3><p>Мы скоро с вами свяжемся.</p>' )
					->set_width( 50 ),
			]
		);
}

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );


function crb_attach_post_options() {
	Container::make( 'post_meta', __( 'Section Options', 'alevel' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/home.php' )
		->add_tab(
			__( 'Первый экран', 'alevel' ),
			[
				Field::make( 'text', 'crb_under_title', __( 'Над заголовком', 'alevel' ) )
					->set_default_value( 'education <span> space</span>' )
					->set_width( 33 ),
				Field::make( 'text', 'crb_sub_title', __( 'Подзаголовок', 'alevel' ) )
					->set_default_value( '<span>Lorem ipsum dolor</span> sit amet, consectetur adipisicing elit' )
					->set_width( 33 ),
				Field::make( 'textarea', 'crb_sub_title_text', __( 'Текст', 'alevel' ) )
					->set_default_value( 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' )
					->set_width( 33 ),
				Field::make( 'text', 'crb_sub_link', __( 'Ссылка', 'alevel' ) )
					->set_default_value( '#' )
					->set_width( 100 ),
			]
		)->add_tab(
			__( 'Второй экран' ),
			[
				Field::make( 'rich_text', 'crb_about_us', __( 'О нас', 'alevel' ) )
					->set_default_value(
						'<h2><span>о нас,<br></span> о пространстве</h2>
                    <p>
                        Когда мы создавали нашу школу, мы в первую очередь думали о результате. Единственным логичным
                        результатом окончания IT курсов является трудоустройство по выбранной специальности. Для
                        достижения поставленной цели
                    </p>'
					)->set_width( 100 ),
				Field::make( 'complex', 'crb_about_us_slider', __( 'Слайдер', 'alevel' ) )->add_fields(
					[
						Field::make( 'image', 'image', __( 'Слайд', 'alevel' ) ),
					]
				),
			]
		);

	Container::make( 'post_meta', __( 'Section Options', 'alevel' ) )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/arenda.php' )
		->add_tab(
			__( 'Первый экран' ),
			[
				Field::make( 'text', 'crb_sub_title', __( 'Подзаголовок', 'alevel' ) )
					->set_default_value( '<span>Lorem ipsum dolor</span> sit amet, consectetur adipisicing elit' )
					->set_width( 50 ),
				Field::make( 'textarea', 'crb_sub_title_text', __( 'Текст', 'alevel' ) )
					->set_default_value( 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' )
					->set_width( 50 ),
			]
		)->add_tab(
			__( 'Второй экран', 'alevel' ),
			[
				Field::make( 'rich_text', 'crb_about_us', __( 'О нас', 'alevel' ) )
					->set_default_value(
						'<h2><span>Кто мы</span> такие?</h2>
                    <p>
                        Когда мы создавали нашу школу, мы в первую очередь думали о результате. Единственным логичным
                        результатом окончания IT курсов является трудоустройство по выбранной специальности. Для
                        достижения поставленной цели
                    </p>'
					)
					->set_width( 100 ),
				Field::make( 'file', 'crb_file', __( 'Bидео', 'alevel' ) )
					->set_width( 50 )
					->set_value_type( 'url' ),
				Field::make( 'image', 'crb_image', __( 'Фото для видео', 'alevel' ) )
					->set_width( 50 )
					->set_value_type( 'url' ),
			]
		)->add_tab(
			__( 'Преимущества' ),
			[
				Field::make( 'complex', 'crb_benefits', __( 'Слайдер', 'alevel' ) )
					->add_fields(
						[
							Field::make( 'image', 'image', __( 'Фото', 'alevel' ) )
								->set_width( 50 ),
							Field::make( 'text', 'crb_under_title', __( 'Заголовок', 'alevel' ) )
								->set_width( 50 )
								->set_default_value( 'КОГДА МЫ СОЗДАВАЛИ НАШУ ШКОЛУ' ),
						]
					),
			]
		)->add_tab(
			__( 'Виды сотрудничества', 'alevel' ),
			[
				Field::make( 'complex', 'crb_benefit_types', __( 'Виды', 'alevel' ) )
					->add_fields(
						[
							Field::make( 'image', 'image', __( 'Фото', 'alevel' ) )
								->set_width( 20 ),
							Field::make( 'text', 'crb_under_title', __( 'Заголовок', 'alevel' ) )
								->set_width( 40 )
								->set_default_value( 'Коворкинг', 'alevel' ),
							Field::make( 'text', 'crb_sub_title', __( 'Подзаголовок', 'alevel' ) )
								->set_width( 40 )
								->set_default_value( 'Директор отдела управленческого консультирования KPMG' ),
						]
					),
			]
		)->add_tab(
			__( 'Цены' ),
			[
				Field::make( 'complex', 'crb_prices', __( 'Цены', 'alevel' ) )
					->add_fields(
						[
							Field::make( 'text', 'crb_price', __( 'Ценa', 'alevel' ) )
								->set_width( 25 )
								->set_default_value( 'Коворкинг' ),
							Field::make( 'text', 'crb_title', __( 'Заголовок', 'alevel' ) )
								->set_width( 25 )
								->set_default_value( 'Коворкинг' ),
							Field::make( 'rich_text', 'crb_content', __( 'услуга', 'alevel' ) )
								->set_width( 25 )
								->set_default_value( '<p>50грн первый час<br> 30 грн последующий</p> ' ),
							Field::make( 'text', 'crb_sub_content', __( 'дополнительный контент', 'alevel' ) )
								->set_width( 25 )
								->set_default_value( 'ПОМИНУТНАЯ ТАРИФИКАЦИЯ' ),
						]
					),
			]
		)->add_tab(
			__( 'ПЛАН АУДИТОРИЙ', 'alevel' ),
			[
				Field::make( 'rich_text', 'crb_plan', __( 'О нас', 'alevel' ) )
					->set_default_value(
						'<h2><span>План аудиторий  </span><br> и расстановки в них <br> посадочных мест </h2>

                    <p>
                        Когда мы создавали нашу школу, мы в первую очередь думали о результате. Единственным логичным
                        результатом окончания IT курсов является трудоустройство по выбранной специальности. Для
                        достижения поставленной цели
                    </p>
                    <p>
                        1 час - <b>1900 грн</b><br>
                        1 час - <b>1900 грн</b>
                    </p>'
					)->set_width( 100 ),
				Field::make( 'image', 'crb_image_plan', __( 'Фото для видео', 'alevel' ) )->set_width( 100 ),
			]
		)->add_tab(
			__( 'Партнеры' ),
			[
				Field::make( 'complex', 'crb_partners', __( 'Слайдер', 'alevel' ) )
					->add_fields(
						[
							Field::make( 'image', 'image', __( 'Слайд', 'alevel' ) ),
						]
					),
			]
		)->add_tab(
			__( 'Галерея', 'alevel' ),
			[
				Field::make( 'text', 'crb_gallery_header', __( 'Заголовок', 'alevel' ) )
					->set_width( 100 ),
				Field::make( 'complex', 'crb_gallery', 'Галерея' )
					->set_layout( 'tabbed-horizontal' )
					->add_fields(
						[
							Field::make( 'image', 'image', __( 'Фото', 'alevel' ) )->set_value_type( 'id' ),
						]
					),
			]
		);

	Container::make( 'post_meta', __( 'Section Options', 'alevel' ) )
		->where( 'post_type', '=', 'post' )
		->add_tab(
			__( 'Первый экран', 'alevel' ),
			[
				Field::make( 'text', 'crb_under_title', __( 'Над заголовком', 'alevel' ) )
					->set_default_value( '23 ноября <span>education space</span>' )
					->set_width( 50 ),
				Field::make( 'text', 'crb_sub_title', __( 'Подзаголовок', 'alevel' ) )
					->set_default_value( '<span>Самая большая </span> в Украине конференция для неайтишников' )
					->set_width( 50 ),

			]
		)->add_tab(
			__( 'О мероприятии', 'alevel' ),
			[
				Field::make( 'rich_text', 'crb_about_event', __( 'О нас', 'alevel' ) )
					->set_default_value(
						'<h2><span>описание </span> ивента</h2>
                    <p>
                        Когда мы создавали нашу школу, мы в первую очередь думали о результате. Единственным логичным
                        результатом окончания IT курсов является трудоустройство по выбранной специальности. Для
                        достижения поставленной цели
                    </p>'
					)->set_width( 100 ),
			]
		)->add_tab(
			__( 'Учителя' ),
			[
				Field::make( 'complex', 'crb_teachers', __( 'Учителя', 'alevel' ) )
					->add_fields(
						[
							Field::make( 'image', 'image', __( 'Фото', 'alevel' ) )
								->set_width( 20 ),
							Field::make( 'text', 'crb_name', __( 'Имя', 'alevel' ) )
								->set_width( 40 )
								->set_default_value( 'Николай Чернов' ),
							Field::make( 'text', 'crb_about', __( 'Подзаголовок', 'alevel' ) )
								->set_width( 40 )
								->set_default_value( 'Директор отдела управленческого консультирования KPMG' ),
						]
					),
			]
		)
		->add_tab(
			__( 'Расписание ', 'alevel' ),
			[
				Field::make( 'complex', 'crb_time', __( 'Расписание ', 'alevel' ) )
					->add_fields(
						[
							Field::make( 'text', 'crb_date', __( 'Время', 'alevel' ) )
								->set_width( 40 )
								->set_default_value( '8.30' ),
							Field::make( 'text', 'crb_title', __( 'Заголовок', 'alevel' ) )
								->set_width( 60 )
								->set_default_value( 'Коворкинг' ),
						]
					),
			]
		)->add_tab(
			__( 'Общее ', 'alevel' ),
			[
				Field::make( 'text', 'crb_date_start', __( 'Время', 'alevel' ) )
					->set_width( 50 )
					->set_default_value( '15.02.1998' ),
				Field::make( 'text', 'crb_time_start', __( 'Начальное время', 'alevel' ) )
					->set_width( 50 )
					->set_default_value( '8.30' ),
				Field::make( 'text', 'crb_cost', __( 'Стоимость', 'alevel' ) )
					->set_default_value( '2000 grn' )
					->set_width( 50 ),
				Field::make( 'text', 'crb_cost_online', __( 'Стоимость online', 'alevel' ) )
					->set_default_value( '2000 grn' )
					->set_width( 50 ),
			]
		);

}

add_action( 'carbon_fields_register_fields', 'crb_attach_post_options' );
