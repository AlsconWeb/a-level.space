jQuery( document ).ready( function() {
	jQuery( '.about-us-slider' ).slick( {
		autoplay: true,
	} );

	jQuery( '.gallery-slider' ).slick( {
		slidesToShow: 2,
		slidesToScroll: 1,
		autoplay: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
		],
	} );

	jQuery( function() {
		var header = jQuery( '#header' );

		jQuery( window ).scroll( function() {
			var scroll = jQuery( window ).scrollTop();
			if ( scroll >= 150 ) {
				header.addClass( 'scrolled' );
			} else {
				header.removeClass( 'scrolled' );
			}
		} );
	} );

	jQuery( '.partners-slider' ).slick( {
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 568,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
		],
	} );

	jQuery( '.phone-mask' ).mask( '+38 (000) 000-0000' );

	function validation( fieldName, fieldValue ) {
	    switch ( fieldName ) {
			case 'name':
				if ( fieldValue.length > 4 ) {
					return true;
				}
				jQuery( 'input[name="your-name"]' ).addClass( 'error' );
				return false;
			case 'email':
				if ( fieldValue.match( /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i ) ) {
					return true;
				}
				jQuery( 'input[name="your-email"]' ).addClass( 'error' );
				return false;
			case 'phone':
				if ( fieldValue.length == 18 ) {
					return true;
				}
				jQuery( 'input[name="tel"]' ).addClass( 'error' );
				return false;
			default:
				return null;
		}
	}

	jQuery( '.modal-close' ).on( 'click', function() {
		jQuery( '.success-modal' ).removeClass( 'success-modal-visible' );
	} );

	jQuery( '.event-form' ).submit( function( event ) {
		jQuery( 'input[name="your-name"]' ).removeClass( 'error' );
		jQuery( 'input[name="your-email"]' ).removeClass( 'error' );
		jQuery( 'input[name="tel"]' ).removeClass( 'error' );

		var sendUrl = jQuery( 'input[name="url"]' ).val();

		var name = jQuery( 'input[name="your-name"]' ).val();
		var email = jQuery( 'input[name="your-email"]' ).val();
		var phone = jQuery( 'input[name="tel"]' ).val();
		var events = jQuery( 'select[name="event"]' ).val();

		var nameValid = validation( 'name', name );
		var emailValid = validation( 'email', email );
		var phoneValid = validation( 'phone', phone );
		var eventValid = true;

		var chats = jQuery( '.data-chats' ).attr( 'data-chats' );

		var online = 'online';
		if ( ! jQuery( '#online' ).prop( 'checked' ) ) {
			online = 'offline';
		}
		var formData = {
			name: name,
			email: email,
			phone: phone,
			event: events,
			chats: chats,
			online: online,
		};

		console.log( 'chats:', chats );

		//console.log( 'formData: ', sendUrl );
		if ( nameValid && emailValid && phoneValid && eventValid ) {
			// ga( 'send', 'event', 'Zayavka', 'Send' );
			jQuery.ajax( {
				type: 'POST',
				url: sendUrl,
				data: formData,

			} ).done( function( ) {
				jQuery( '.success-modal' ).addClass( 'success-modal-visible' );
				jQuery( 'input[name="your-name"]' ).val( '' );
				jQuery( 'input[name="your-email"]' ).val( '' );
				jQuery( 'input[name="tel"]' ).val( '' );
			} );
		}
		// stop the form from submitting the normal way and refreshing the page
		event.preventDefault();
	} );


	jQuery( '.arenda-form' ).submit( function( event ) {
		jQuery( 'input[name="your-name"]' ).removeClass( 'error' );
		jQuery( 'input[name="your-email"]' ).removeClass( 'error' );
		jQuery( 'input[name="tel"]' ).removeClass( 'error' );

		var sendUrl = jQuery( 'input[name="url"]' ).val();

		var name = jQuery( 'input[name="your-name"]' ).val();
		var email = jQuery( 'input[name="your-email"]' ).val();
		var phone = jQuery( 'input[name="tel"]' ).val();

		var nameValid = validation( 'name', name );
		var emailValid = validation( 'email', email );
		var phoneValid = validation( 'phone', phone );

		var chats = jQuery( '.data-chats' ).attr( 'data-chats' );

		var formData = {
			name: name,
			email: email,
			phone: phone,
			chats: chats,
		};

		//console.log( 'chats:', chats );

		//console.log( 'formData: ', sendUrl );
		if ( nameValid && emailValid && phoneValid ) {
			// ga( 'send', 'event', 'Arenda', 'Send' );
			jQuery.ajax( {
				type: 'POST',
				url: sendUrl,
				data: formData,

			} ).done( function( ) {
				jQuery( '.success-modal' ).addClass( 'success-modal-visible' );
				jQuery( 'input[name="your-name"]' ).val( '' );
				jQuery( 'input[name="your-email"]' ).val( '' );
				jQuery( 'input[name="tel"]' ).val( '' );
			} );
		}
		// stop the form from submitting the normal way and refreshing the page
		event.preventDefault();
	} );

	jQuery( '.menu-modal-close' ).on( 'click', function() {
		jQuery( '.menu-modal' ).removeClass( 'menu-modal-visible' );
	} );

	jQuery( '.mob-menu-btn' ).on( 'click', function() {
		jQuery( '.menu-modal' ).addClass( 'menu-modal-visible' );
	} );
} );
