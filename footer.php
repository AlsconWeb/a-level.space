<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package A-Level_space
 */

/**
 * Created 09.11.2021
 * Version 1.0.0
 * Last update 09.11.2021
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */
?>

</div> <!-- .site-content -->
<footer id="footer" class="footer">
	<div class="container">
		<h5>
			<?php echo wp_kses_post( carbon_get_theme_option( 'crb_address' ) ); ?>
		</h5>
		<hr>

		<ul class="contact">
			<li>Телефон Харьков:
				<a href="tel: <?php echo esc_attr( carbon_get_theme_option( 'crb_phone' ) ); ?>">
					<?php echo esc_html( carbon_get_theme_option( 'crb_phone' ) ); ?>
				</a>
			</li>
			<li>Телефон Киев:
				<a href="tel: <?php echo esc_attr( carbon_get_theme_option( 'crb_phone_kiev' ) ); ?>">
					<?php echo esc_html( carbon_get_theme_option( 'crb_phone_kiev' ) ); ?>
				</a>
			</li>
			<li class="mail">Email:
				<a href="mailto: <?php echo esc_attr( carbon_get_theme_option( 'crb_email' ) ); ?>">
					<?php echo esc_html( carbon_get_theme_option( 'crb_email' ) ); ?>
				</a>
			</li>
		</ul>

		<ul class="socials">
			<li>
				<a
						href="<?php echo esc_url( carbon_get_theme_option( 'crb_instagram' ) ); ?>"
						class="instagram"
						target="_blank">
					<i class="fab fa-instagram"></i>
				</a>
			</li>
			<li>
				<a
						href="<?php echo esc_url( carbon_get_theme_option( 'crb_telegram_s' ) ); ?>"
						class="telegram"
						target="_blank">
					<i class="fab fa-telegram-plane"></i>
				</a>
			</li>
			<li>
				<a
						href="<?php echo esc_url( carbon_get_theme_option( 'crb_facebook' ) ); ?>"
						class="facebook"
						target="_blank">
					<i class="fab fa-facebook-f"></i>
				</a>
			</li>
		</ul>

		<a href="<?php echo esc_url( get_home_url() ); ?>" class="logo">
			<img
					src="<?php echo esc_url( get_template_directory_uri() . '/build/images/alevelLogo.png' ); ?>"
					alt="Logo">
		</a>
	</div>
</footer><!-- #footer -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
