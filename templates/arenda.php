<?php
/**
 * Template Name: Arenda page
 *
 * @package A-Level_space
 */

/**
 * Created 09.11.2021
 * Version 1.0.0
 * Last update 09.11.2021
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

get_header();
?>

	<main id="main" class="site-main">

		<section class="first-screen">
			<div class="container">
				<div class="content">
					<?php the_content(); ?>

					<span class="sub-title">
						<?php echo wp_kses_post( carbon_get_the_post_meta( 'crb_sub_title' ) ); ?>
					</span>

					<p><?php echo wp_kses_post( carbon_get_the_post_meta( 'crb_sub_title_text' ) ); ?></p>


					<ul class="socials">
						<li>
							<a
									href="<?php echo esc_url( carbon_get_theme_option( 'crb_instagram' ) ); ?>"
									target="_blank">Instagram</a>
						</li>
						<li>
							<a
									href="<?php echo esc_url( carbon_get_theme_option( 'crb_telegram_s' ) ); ?>"
									target="_blank">Telegram</a>
						</li>
						<li>
							<a
									href="<?php echo esc_url( carbon_get_theme_option( 'crb_facebook' ) ); ?>"
									target="_blank">Facebook</a>
						</li>
					</ul>
				</div>

			</div>
		</section><!-- .first-screen -->

		<section class="second-screen">
			<div class="container">
				<div class="left">
					<img
							src="<?php echo esc_url( carbon_get_the_post_meta( 'crb_image' ) ); ?>"
							alt="About arenda"/>
				</div>
				<div class="right">
					<span class="line">We love <span> creating</span></span>

					<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_about_us' ) ) ); ?>
				</div>

				<div class="center">
					<span class="line">We love <span> creating</span></span>

					<h2>Преимущества </h2>

					<div class="icon-list">
						<?php
						$benefits = carbon_get_the_post_meta( 'crb_benefits' );

						foreach ( $benefits as $benefit ) :
							?>
							<div class="icon-item">
								<?php echo wp_get_attachment_image( $benefit['image'], 'full' ); ?>
								<h6>
									<?php echo wp_kses_post( $benefit['crb_under_title'] ); ?>
								</h6>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</section><!-- .second-screen -->

		<section class="third-screen">
			<div class="container">
				<span class="line">We love <span> creating</span></span>

				<h2><span>Виды сотрудничества <br> </span> с преимуществами </h2>

				<div class="benefits-list">
					<?php
					$benefit_types = carbon_get_the_post_meta( 'crb_benefit_types' );

					foreach ( $benefit_types as $benefit_type ) :
						?>
						<div class="single-benefit">
							<?php echo wp_get_attachment_image( $benefit_type['image'], 'full' ); ?>
							<h4>
								<?php echo esc_html( $benefit_type['crb_under_title'] ); ?>
							</h4>
							<hr>
							<p><?php echo wp_kses_post( $benefit_type['crb_sub_title'] ); ?></p>
						</div>
					<?php endforeach; ?>
				</div>
			</div>

		</section><!-- .third-screen -->

		<section class="fourth-screen">
			<div class="container">
				<span class="line">We love <span> creating</span></span>

				<h2>Цены</h2>

				<div class="price-list">
					<?php
					$prices = carbon_get_the_post_meta( 'crb_prices' );

					foreach ( $prices as $price ) :
						?>
						<div class="single-price">
							<span class="price"><?php echo esc_html( $price['crb_price'] ); ?></span>
							<h4><?php echo esc_html( $price['crb_title'] ); ?></h4>
							<hr>
							<?php echo wp_kses_post( wpautop( $price['crb_content'] ) ); ?>
							<hr/>

							<span class="dop">
								<?php echo esc_html( $price['crb_sub_content'] ); ?>
							</span>
							<a href="#form-screen" class="button">
								Заказать
							</a>
						</div>

					<?php endforeach; ?>

				</div>
			</div>
		</section>
		<!-- .fourth-screen -->

		<section class="fifth-screen info-screen">
			<div class="container">
				<div class="left">
					<?php echo wp_get_attachment_image( carbon_get_the_post_meta( 'crb_image_plan' ), 'full' ); ?>
				</div>
				<div class="right">
					<span class="line">We love <span> creating</span></span>

					<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_plan' ) ) ); ?>

				</div>
			</div>
		</section>
		<!-- .fifth-screen -->

		<?php get_template_part( 'template-parts/content', 'gallery' ); ?>
		<?php get_template_part( 'template-parts/content', 'form-arenda' ); ?>
	</main><!-- #main -->

<?php
get_footer();
