<?php
/*
Template Name: Home page
*/


get_header();
?>

    <main id="main" class="site-main">

        <section class="first-screen">
            <div class="container">
                <div class="content">
                    <span class="line"><?php echo carbon_get_the_post_meta('crb_under_title') ?></span>
                    <?php the_content(); ?>

                    <span class="sub-title">
                        <?php echo carbon_get_the_post_meta('crb_sub_title') ?>
                    </span>
                    <p><?php echo carbon_get_the_post_meta('crb_sub_title_text') ?></p>
                    <a href="<?php echo carbon_get_the_post_meta('crb_sub_link') ?>" class="button">Подробнее</a>

                    <ul class="socials">
                        <li>
                            <a href="<?php echo carbon_get_theme_option('crb_instagram') ?>"
                               target="_blank">Instagram</a>
                        </li>
                        <li>
                            <a href="<?php echo carbon_get_theme_option('crb_telegram_s') ?>"
                               target="_blank">Telegram</a>
                        </li>
                        <li>
                            <a href="<?php echo carbon_get_theme_option('crb_facebook') ?>" target="_blank">Facebook</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section><!-- .first-screen -->

        <section id="second-screen" class="second-screen">
            <div class="container">
                <div class="left">
                    <div class="about-us-slider">
                        <?php
                        $slider = carbon_get_the_post_meta('crb_about_us_slider');

                        foreach ($slider as $slide) {
                            echo wp_get_attachment_image($slide["image"], 'full');
                        }
                        ?>
                    </div>
                </div>
                <div class="right">
                    <span class="line">We love <span> creating</span></span>

                    <?php echo wpautop(carbon_get_the_post_meta('crb_about_us')); ?>
                </div>
            </div>
        </section><!-- .second-screen -->

        <section class="third-screen">
            <div class="container">
                <span class="line">We love <span> creating</span></span>
                <h2><span>Анонсы</span> мероприятий</h2>

                <?php
                $the_query = new WP_Query(array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                ));
                ?>

                <?php if ($the_query->have_posts()) : ?>
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <div class="single-event">
                            <div class="event-image">
                                <?php echo the_post_thumbnail('full'); ?>
                            </div>
                            <div class="event-content">
                                <?php echo '<h4>' . esc_html(get_the_title()) . '</h4>'; ?>

                                <div class="date-info">
                                    <div class="left">
                                        <span class="calendar"><?php echo carbon_get_the_post_meta('crb_date_start') ?></span>
                                        <span class="oclock"><?php echo carbon_get_the_post_meta('crb_time_start') ?></span>
                                    </div>

                                    <div class="right">
                                        <?php
                                        $online = carbon_get_the_post_meta('crb_cost_online');
                                        $offline = carbon_get_the_post_meta('crb_cost');
                                        if ($offline) {
                                            ?>
                                            <div class="cost-place">
                                                <span class="cost">Стоимость: <span><?php echo $offline ?></span></span>
                                                <?php
                                                if($online){
                                                    echo '<span class="cost online">Стоимость online: <span>' . $online . '</span></span>';
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="cost-place">
                                                <span class="cost"><span>Бесплатно</span></span>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>

                                </div>

                                <?php the_excerpt(); ?>
                                <a href="<?php echo get_permalink(); ?>" target="_blank" class="detail">Подробнее</a>
                            </div>
                        </div>

                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php else : ?>
                    <p><?php __('No News'); ?></p>
                <?php endif; ?>

                <a href="<?php echo get_permalink(get_option('page_for_posts')); ?>" class="button">Все мероприятия</a>
            </div>
        </section><!-- .third-screen -->

        <?php
        get_template_part('template-parts/content', 'gallery');
        ?>

    </main><!-- #main -->

<?php
get_footer();