<?php
/**
 * Template part for displaying a gallery
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package A-Level_space
 */

$slides   = carbon_get_the_post_meta( 'crb_gallery' );
$headline = carbon_get_the_post_meta( 'crb_gallery_header' );
if ( empty( $slides ) ) :
	?>

	<section class="gallery-screen">
		<div class="container">
			<span class="line">We love <span> creating</span></span>
			<h2><span>галерея</span> фото</h2>

			<div class="gallery">

				<div
						class="gallery1"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image1' ) ); ?>)
								"></div>
				<div
						class="gallery2"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image2' ) ); ?>)
								"></div>
				<div
						class="gallery3"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image3' ) ); ?>)"></div>
				<div
						class="gallery4"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image4' ) ); ?>)"></div>
				<div
						class="gallery5"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image5' ) ); ?>)"></div>
				<div
						class="gallery6"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image6' ) ); ?>)"></div>
				<div
						class="gallery7"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image7' ) ); ?>)"></div>

			</div>

			<div class="gallery-slider">
				<div
						class="gallery-img"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image1' ) ); ?>)"></div>
				<div
						class="gallery-img"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image2' ) ); ?>)"></div>
				<div
						class="gallery-img"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image3' ) ); ?>)"></div>
				<div
						class="gallery-img"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image4' ) ); ?>)
								"></div>
				<div
						class="gallery-img"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image5' ) ); ?>)"></div>
				<div
						class="gallery-img"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image6' ) ); ?>)"></div>
				<div
						class="gallery-img"
						style="background-image: url(<?php echo esc_url( carbon_get_theme_option( 'crb_image7' ) ); ?>)"></div>
			</div>
		</div>
	</section><!-- .gallery-screen -->
<?php else : ?>
	<section class="gallery-screen">
		<div class="container">
			<?php echo wp_kses_post( $headline ) ?? ''; ?>
			<div class="gallery">
				<?php foreach ( $slides as $key => $slide ) : ?>
					<div
							class="gallery<?php echo esc_attr( $key ); ?>"
							style="background-image: url(<?php echo esc_url( wp_get_attachment_image_url( $slide['image'], 'full' ) ); ?>)">
					</div>
				<?php endforeach; ?>
			</div>
			<div class="gallery-slider">
				<?php foreach ( $slides as $slide ) : ?>
					<div
							class="gallery-img"
							style="background-image: url(<?php echo esc_url( wp_get_attachment_image_url( $slide['image'], 'full' ) ); ?>)"></div>
				<?php endforeach; ?>
			</div>
		</div>
	</section><!-- .gallery-screen -->
<?php
endif;



