<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package A-Level_space
 */

?>

<div class="single-event">
    <div class="event-image">
        <?php echo the_post_thumbnail('full'); ?>
    </div>
    <div class="event-content">
        <?php echo '<h4>' . esc_html(get_the_title()) . '</h4>'; ?>
        <div class="date-info">
            <div class="left">
                <span class="calendar"><?php echo carbon_get_the_post_meta('crb_date_start') ?></span>
                <span class="oclock"><?php echo carbon_get_the_post_meta('crb_time_start') ?></span>
            </div>

            <div class="right">
                <?php
                $online = carbon_get_the_post_meta('crb_cost_online');
                $offline = carbon_get_the_post_meta('crb_cost');
                if ($offline) {
                    ?>
                    <div class="cost-place">
                        <span class="cost">Стоимость: <span><?php echo $offline ?></span></span>
                        <?php
                        if($online){
                            echo '<span class="cost online">Стоимость online: <span>' . $online . '</span></span>';
                        }
                        ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="cost-place">
                        <span class="cost"><span>Бесплатно</span></span>
                    </div>
                    <?php
                }
                ?>
            </div>

        </div>
        <?php the_excerpt(); ?>
        <a href="<?php echo get_permalink(); ?>" target="_blank" class="detail">Подробнее</a>
    </div>
</div>
<!-- #post-<?php the_ID(); ?> -->
