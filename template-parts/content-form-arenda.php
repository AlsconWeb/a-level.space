<?php
/**
 * Template part for displaying a form section
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package A-Level_space
 */
$chats = carbon_get_theme_option( 'crb_telegram' );
?>

<section id="form-screen" class="form-screen">
	<div class="container">
		<span class="line">We love <span> creating</span></span>
		<h2>Зарегистрироваться</h2>

		<form class="arenda-form">
			<span class="data-chats" data-chats='<?php echo json_encode( $chats ); ?>'></span>
			<input type="hidden" name="url" id="ajax_url"
				   value="<?php echo get_template_directory_uri() . '/mail/mail_arenda.php'; ?>"/>

			<div class="form-row">
				<input type="text" name="your-name" value="" size="40" aria-required="true" aria-invalid="false"
					   placeholder="ФИО">

				<input type="email" name="your-email" value="" size="40" aria-required="true" aria-invalid="false"
					   placeholder="E-mail">
			</div>
			<div class="form-row">
				<input type="tel" name="tel" class="phone-mask" value="" size="40" aria-required="true"
					   aria-invalid="false" placeholder="+380">
			</div>
			<button type="submit" class="button">Зарегистрироваться</button>
		</form>

		<div class="success-modal" id="arenda-form">
			<div class="modal-content">
				<div class="modal-close"><i class="fas fa-times"></i></div>
				<?php echo wpautop( carbon_get_theme_option( 'arenda_form' ) ); ?>
			</div>
		</div>
	</div>
</section><!-- .form-screen -->


