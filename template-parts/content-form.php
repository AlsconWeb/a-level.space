<?php
/**
 * Template part for displaying a form section
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package A-Level_space
 */
$post_id = get_the_ID();
$chats   = carbon_get_theme_option( 'crb_telegram' );
?>

<section id="form-screen" class="form-screen">
	<div class="container">
		<span class="line">We love <span> creating</span></span>
		<h2>Зарегистрироваться</h2>

		<form class="event-form">
			<span class="data-chats" data-chats='<?php echo json_encode( $chats ); ?>'></span>
			<input type="hidden" name="url" id="ajax_url"
				   value="<?php echo get_template_directory_uri() . '/mail/mail.php'; ?>"/>

			<div class="form-row">
				<input type="text" name="your-name" value="" size="40" aria-required="true" aria-invalid="false"
					   placeholder="ФИО">

				<input type="email" name="your-email" value="" size="40" aria-required="true" aria-invalid="false"
					   placeholder="E-mail">
			</div>
			<div class="form-row">
				<input type="tel" name="tel" class="phone-mask" value="" size="40" aria-required="true"
					   aria-invalid="false" placeholder="+380">
				<select name="event" aria-required="true" aria-invalid="false">
					<?php
					$the_query = new WP_Query( [
						'post_type'      => 'post',
						'posts_per_page' => 30,
						'cat'            => '1',
					] );
					?>
					<?php if ( $the_query->have_posts() ) : ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<option <?php if ( $post_id == get_the_ID() ) { ?> selected <?php } ?>
									value="<?php echo get_the_title() ?>"><?php echo get_the_title() ?></option>

						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>
					<?php else : ?>
						<p><?php __( 'No News' ); ?></p>
					<?php endif; ?>
				</select>

			</div>
			<?php
			$online = carbon_get_the_post_meta( 'crb_cost_online' );
			?>
			<div class="form-row <?php if ( ! $online ) {
				echo 'hidden';
			} ?>">
				<div class="form-group">
					<input type="checkbox" id="online" name="online">
					<label for="online">online</label>
				</div>
			</div>
			<button type="submit" class="button">Зарегистрироваться</button>
		</form>

		<div class="success-modal" id="event-form">
			<div class="modal-content">
				<div class="modal-close"><i class="fas fa-times"></i></div>
				<?php echo wpautop( carbon_get_theme_option( 'event_form' ) ); ?>

			</div>
		</div>
	</div>
</section><!-- .form-screen -->


