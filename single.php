<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package A-Level_space
 */

get_header();
?>

    <main id="main" class="site-main">

        <?php
        while (have_posts()) :
            the_post();
            ?>

            <section class="first-screen">
                <div class="container">
                    <div class="content">
                        <span class="line"><?php echo carbon_get_the_post_meta('crb_under_title') ?></span>
                        <h1><?php echo get_the_title() ?></h1>
                        <span class="sub-title"><?php echo carbon_get_the_post_meta('crb_sub_title') ?></span>

                        <?php the_content() ?>

                        <?php
                        $online = carbon_get_the_post_meta('crb_cost_online');
                        $offline = carbon_get_the_post_meta('crb_cost');
                        if ($offline) {
                            ?>
                            <span class="cost">Стоимость: <span><?php echo $offline ?></span></span>
                            <?php
                            if ($online) {
                                echo '<span class="cost online">Стоимомть online: <span>' . $online . '</span></span>';
                            }
                            ?>
                            <?php
                        } else {
                            ?>
                            <span class="cost">Стоимоcть: <span>Бесплатно</span></span>

                            <?php
                        }
                        ?>

                        <a href="#form-screen" class="button">записаться</a>

                        <ul class="socials">
                            <li>
                                <a href="<?php echo carbon_get_theme_option('crb_instagram') ?>" target="_blank">Instagram</a>
                            </li>
                            <li>
                                <a href="<?php echo carbon_get_theme_option('crb_telegram_s') ?>" target="_blank">Telegram</a>
                            </li>
                            <li>
                                <a href="<?php echo carbon_get_theme_option('crb_facebook') ?>"
                                   target="_blank">Facebook</a>
                            </li>
                        </ul>

                        <?php echo the_post_thumbnail('full'); ?>
                    </div>

                </div>
            </section><!-- .first-screen -->

            <section class="second-screen">
                <div class="container">
                    <span class="line">We love <span> creating</span></span>
                    <?php echo wpautop(carbon_get_the_post_meta('crb_about_event')); ?>
                </div>
            </section><!-- .first-screen -->


            <section class="third-screen">
                <div class="container">
                    <hr>
                    <h2>Спикеры</h2>

                    <div class="benefits-list">
                        <?php
                        $teachers = carbon_get_the_post_meta('crb_teachers');

                        foreach ($teachers as $teacher) { ?>
                            <div class="single-benefit">
                                <?php echo wp_get_attachment_image($teacher["image"], 'full'); ?>
                                <h4>
                                    <?php echo $teacher["crb_name"] ?>
                                </h4>
                                <hr>
                                <p><?php echo $teacher["crb_about"] ?></p>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <!--            <div class="container route">-->
                <!--                <hr>-->
                <!--                <h2><span>расписание</span> событий конференции</h2>-->
                <!--                <div class="routes-list">-->
                <!---->
                <!--                    --><?php
                //                    $routes = carbon_get_the_post_meta('crb_time');
                //
                //                    foreach ($routes as $route) {
                ?>
                <!--                        <div class="route-item">-->
                <!--                            <div class="left">-->
                <!--                                <span>--><?php //echo $route["crb_date"]
                ?><!--</span>-->
                <!--                            </div>-->
                <!---->
                <!--                            <div class="right"><span>--><?php //echo $route["crb_title"]
                ?><!--</span></div>-->
                <!---->
                <!--                        </div>-->
                <!--                        --><?php
                //                    }
                //
                ?>
                <!---->
                <!--                </div>-->
                <!--            </div>-->
            </section>

            <?php get_template_part('template-parts/content', 'form'); ?>

        <?php


        endwhile; // End of the loop.
        ?>

    </main><!-- #main -->

<?php
get_footer();
